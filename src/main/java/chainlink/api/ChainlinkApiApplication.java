
package chainlink.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChainlinkApiApplication {
	public static void main(String[] args) {
		SpringApplication.run(ChainlinkApiApplication.class, args);
	}
}
